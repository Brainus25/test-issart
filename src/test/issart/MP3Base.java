package test.issart;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.os.Environment;
import android.util.Log;

import java.util.LinkedList;
import java.util.List;

/**
 * Created by User on 30.01.2015.
 */
public class MP3Base
{
    static private MP3Base instance;
    private Listener listener;
    final DBHelper helper;

    public static final int S_NOTINITED = 0;
    public static final int S_DOWNLOADING_LIST = 1;
    public static final int S_DOWNLOADING_LIST_FAILED = 2;
    public static final int S_ERROR = 3;
    public static final int S_INITED = 4;

    int status = S_NOTINITED; //S_*

    static public interface Listener
    {
        public void onStatusChanged(int status);
    }

    public static class DBHelper extends SQLiteOpenHelper
    {

        private static final int DB_VERSION = 1;
        SQLiteDatabase db;

        public DBHelper(Context context)
        {
            super(context, "mp3db", null, DB_VERSION);
            db = getWritableDatabase();//TODO надо проверять что DB успешно открылась и оповещать есть что.
        }

        @Override
        public void onCreate(SQLiteDatabase db)
        {
            createDB(db);
        }

        @Override
        public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion)
        {
            dropAllTables(db);
            onCreate(db);
        }

        synchronized public static void dropAllTables(SQLiteDatabase db)
        {
            db.execSQL("DROP TABLE IF EXISTS mp3;");
        }

        synchronized void eraseDB()
        {
            db.delete("mp3", null, null);
        }

        synchronized void createDB(SQLiteDatabase db)
        {
            db.execSQL("CREATE TABLE IF NOT EXISTS mp3 (url TEXT PRIMARY KEY NOT NULL,description TEXT,filepath TEXT NOT NULL);");
        }

        synchronized void finish()
        {
            db.close();
        }
    }

    public static synchronized MP3Base getInstance(Context context, Listener listener)
    {
        if(instance == null)
            instance = new MP3Base(context, listener);
        else
            instance.listener = listener;
        return instance;
    }

    private MP3Base(Context context, Listener listener)
    {
        this.listener = listener;
        setStatus(S_NOTINITED);
        helper = new DBHelper(context);
        checkList();
    }

    String getFilePlace()
    {
        return Environment.getExternalStorageDirectory().toString() + "/" + "MP3TEST";//TODO надо делать проверку что SD доступна, и выбирать другое место для харнения либо генерить ошибку
    }

    private void checkList()
    {
        new Thread()
        {
            @Override
            public void run()
            {
                synchronized(helper)
                {
                    Cursor c = null;
                    try
                    {
                        c = helper.db.rawQuery("SELECT COUNT(*) from mp3", null);
                        c.moveToFirst();
//                        Log.e("test", "c=" + c.getInt(0));
//                        c.getInt(0);
                        if(c.getInt(0) == 0)
                        {
                            setStatus(S_DOWNLOADING_LIST);
                            downloadMP3List();
                        }
                        else
                            setStatus(S_INITED);
                    }
                    catch(Exception e)
                    {
                        setStatus(S_ERROR);
                        Log.e("test", "", e);
                    }
                    finally
                    {
                        if(c != null)
                            c.close();
                    }
                }

            }
        }.start();
    }

    private void setStatus(int newStatus)
    {
        if(newStatus != status)
        {
            status = newStatus;
            if(listener != null)
                listener.onStatusChanged(status);
        }
    }

    private void downloadMP3List()
    {
        List<String> mp3List = Util.download("http://46.19.35.33/mp3list.txt");
        if(mp3List == null)
        {
            setStatus(S_DOWNLOADING_LIST_FAILED);
            return;
        }
        //TODO тут надо проверить что там реально пришло, там могут оказаться совершенно левые данные, не урлы.
        String filePlace = getFilePlace();
        try
        {
            synchronized(helper)
            {
                for(String s : mp3List)
                {
                    ContentValues contentValues = new ContentValues(2);
                    contentValues.put("url", s);
                    contentValues.put("filepath", filePlace + "/" + System.nanoTime() + ".mp3");
                    helper.db.insert("mp3", null, contentValues);
                }
            }
            setStatus(S_INITED);
        }
        catch(Exception e)
        {
            setStatus(S_DOWNLOADING_LIST_FAILED);
            Log.e("test", "", e);
        }
//        Log.w("test", "mp3List=" + mp3List);
    }

    public interface PlayListCallback
    {
        void complete(List<String> mp3PlayList);
    }

    public void requestPlayList(final PlayListCallback cb)
    {
        if(cb == null)
            return;

        new Thread()
        {
            @Override
            public void run()
            {
                synchronized(helper)
                {
                    Cursor c = null;
                    try
                    {
                        List<String> result = new LinkedList<String>();
                        c = helper.db.query("mp3", new String[]{"url"}, null, null, null, null, null);
                        if(c.moveToFirst())
                            do
                            {
                                result.add(c.getString(0));
                            }
                            while(c.moveToNext());
                        cb.complete(result);
                    }
                    catch(Exception e)
                    {
                        cb.complete(null);
                    }
                    finally
                    {
                        if(c != null)
                            c.close();
                    }
                }
            }
        }.start();
    }
}
