package test.issart;

/**
 * Created by Vlad on 03.02.2015.
 */
public class MP3Description
{
    String url;
    String descr;
    String filePath;

    public MP3Description(String id)
    {
        url = id;
    }

    public String getTextDescription()
    {
        if(descr != null)
            return descr;
        if(url != null)
            return url;
        return "Пусто";
    }
}
