package test.issart;

import android.util.Log;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

/**
 * Created by User on 30.01.2015.
 */
public class Util
{
    public static List<String> download(String urlStr)
    {
        Log.i("test", "download " + urlStr);
        HttpURLConnection connection = null;
        try
        {
            URL url = new URL(urlStr);
            connection = (HttpURLConnection) url.openConnection();

            connection.setRequestMethod("GET");
            connection.setConnectTimeout(10000);
            connection.connect();

            int responseCode = connection.getResponseCode();
            Log.i("test", "download responseCode=" + responseCode);
            if(responseCode != 200)
                return null;

            InputStream is = connection.getInputStream();
            InputStreamReader isr = new InputStreamReader(is);

            BufferedReader reader = new BufferedReader(isr, 8192);
            List<String> result = new LinkedList<String>();
            String line;
            while((line = reader.readLine()) != null)
            {
                result.add(line);
            }
            return result;
        }
        catch(Exception e)
        {
            Log.e("test", "", e);
            return null;
        }
        finally
        {
            if(connection != null)
                connection.disconnect();
        }

    }

}
