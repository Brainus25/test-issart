package test.issart;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Vlad on 03.02.2015.
 */
public class MP3ListAdapter extends BaseAdapter
{
    ArrayList<MP3Description> list = new ArrayList<MP3Description>();

    public void set(List<String> newList)
    {
        list.clear();
        if(newList != null)
            for(String s : newList)
            {
                list.add(new MP3Description(s));
            }
        notifyDataSetChanged();
    }


    @Override
    public int getCount()
    {
        return list.size();
    }

    @Override
    public Object getItem(int position)
    {
        return list.get(position);
    }

    @Override
    public long getItemId(int position)
    {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent)
    {
        Holder h;
        if(convertView == null)
        {
            LayoutInflater inflater = LayoutInflater.from(parent.getContext());

            convertView = inflater.inflate(R.layout.mp3list_item, parent, false);
            h = new Holder();
            h.tvText = (TextView) convertView.findViewById(R.id.tvText);

            convertView.setTag(h);
        }
        else
        {
            h = (Holder) convertView.getTag();
        }

        MP3Description mp3Description = (MP3Description) getItem(position);

        h.tvText.setText(mp3Description.getTextDescription());

        return convertView;
    }

    static class Holder
    {
        public TextView tvText;
    }
}
