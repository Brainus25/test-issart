package test.issart;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ListView;
import android.widget.TextView;

import java.util.List;

public class Act extends Activity
{
    TextView tvStatus;
    ListView lvMP3List;
    MP3ListAdapter mp3ListAdapter;
    MP3Base mp3Base;

    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);
        tvStatus = (TextView) findViewById(R.id.tvStatus);
        lvMP3List = (ListView) findViewById(R.id.lvMP3List);

        mp3ListAdapter = new MP3ListAdapter();
        lvMP3List.setAdapter(mp3ListAdapter);

        mp3Base = MP3Base.getInstance(getApplicationContext(), mp3DBlistener);
    }

    MP3Base.Listener mp3DBlistener = new MP3Base.Listener()
    {
        @Override
        public void onStatusChanged(final int status)
        {
            runOnUiThread(new Runnable()
            {
                @Override
                public void run()
                {
                    switch(status)
                    {
                        case MP3Base.S_NOTINITED:
                            tvStatus.setVisibility(View.VISIBLE);
                            tvStatus.setText("Инициализация");
                            break;
                        case MP3Base.S_DOWNLOADING_LIST:
                            tvStatus.setVisibility(View.VISIBLE);
                            tvStatus.setText("Скачивается список");
                            break;
                        case MP3Base.S_DOWNLOADING_LIST_FAILED:
                            tvStatus.setVisibility(View.VISIBLE);
                            tvStatus.setText("Ошибка скачивания списка");
                            break;
                        case MP3Base.S_ERROR:
                            tvStatus.setVisibility(View.VISIBLE);
                            tvStatus.setText("Ошибка БД");
                            break;
                        case MP3Base.S_INITED:
                            tvStatus.setVisibility(View.VISIBLE);
                            tvStatus.setText("Подготовка списка");
                            mp3Base.requestPlayList(new MP3Base.PlayListCallback()
                            {
                                @Override
                                public void complete(List<String> mp3PlayList)
                                {
                                    fillList(mp3PlayList);
                                }
                            });
                            break;
                    }
                }
            });
        }
    };

    void fillList(List<String> mp3PlayList)
    {
        if(mp3PlayList == null)
        {
            tvStatus.setVisibility(View.VISIBLE);
            tvStatus.setText("Ошибка БД");
            return;
        }
        if(mp3PlayList.size() == 0)
        {
            tvStatus.setVisibility(View.VISIBLE);
            tvStatus.setText("Список пуст");
            return;
        }
        tvStatus.setVisibility(View.GONE);
        mp3ListAdapter.set(mp3PlayList);

        Log.e("test", "fillList");
    }
}
